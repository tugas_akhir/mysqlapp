from app.controllers import customers_controller
from flask import Blueprint, request, jsonify

customers_bp = Blueprint("customers_router", __name__)

@customers_bp.route("/users", methods=["GET"])
def shows():
    return customers_controller.shows()

@customers_bp.route("/user", methods=["GET"])
def show():
    params = request.json
    return customers_controller.show(**params)

@customers_bp.route("/users/insert", methods=["POST"])
def insert():
    params = request.json
    return customers_controller.insert(**params)

@customers_bp.route("/user/delete/", methods=["POST"])
def delete():
    params = request.json
    return customers_controller.delete(**params)

@customers_bp.route("/user/update", methods=["POST"])
def update():
    params = request.json
    return customers_controller.update(**params)

@customers_bp.route("/user/requesttoken", methods=["GET"])
def reqtoken():
    params = request.json
    return customers_controller.token(**params)

@customers_bp.errorhandler(404) 
def invalid_route(e): 
    return jsonify({"Invalid route."})
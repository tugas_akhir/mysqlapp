from app.controllers import order_controllers
from flask import Blueprint, request

order_bp = Blueprint("order_routers", __name__)

@order_bp.route("/orders", methods=["GET"])
def showOrders():
    return order_controllers.shows()

@order_bp.route("/order/insert", methods=["POST"])
def insert():
    params = request.json
    return order_controllers.insert(**params)

@order_bp.route("/orders/status", methods=["POST"])
def status():
    return order_controllers.status()
from flask import Flask
from config import Config
from flask_jwt_extended import JWTManager
from app.routers.customers_router import customers_bp
from app.routers.order_routers import order_bp

app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)

app.register_blueprint(customers_bp)
app.register_blueprint(order_bp)
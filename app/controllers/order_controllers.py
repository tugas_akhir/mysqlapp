from app.models.order_models import database as db
from app.models.customers_model import database as custom_db
from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity 
import json, datetime

mysqldbs = db()

@jwt_required()
def shows():
    params = get_jwt_identity()
    dbsresult = mysqldbs.showOrders(**params)
    result = []
    if dbsresult is not None:
        for item in dbsresult:
            id = json.dumps({"id":item[4]})
            productdetails = getProductByID(id)
            user = {
                "username" : item[0],
                "orderid" : item[1],
                "orderdate" : item[2],  
                "productid" : item[4],
                "productname" : item[5],
                "quantityproduct" : productdetails["quantityproduct"],
                "productprice" : productdetails["productprice"],
                "producttype" : productdetails["producttype"],
                "quantityproduct" : productdetails["producttype"]
            }
            result.append(user)
        else:
            result = productdetails
    return jsonify(result)
@jwt_required
def insert(**params):
    token = get_jwt_identity()
    userid = custom_db.showUserByEmail(**token)[0]
    orderdate = datetime.datetime.now().isoformat()
    id = json.dumps({"id":params["_id"]})
    productname = getProductByID(id)["productname"]
    params.update(
        {
            "id" : userid,
            "orderdate" : orderdate,
            "productname" : productname,
            "isready" : 1 
        }
    )
    mysqldbs.insertOrder(**params)
    mysqldbs.commit()
    return jsonify({"massage":"success"})

@jwt_required
def status():
    mysqldbs.updateOrder(**params)
    mysqldbs.commit()
    return jsonify({"massage":"success"})

def getProductByID(data):
    product_data =  request.get(url="https://localhost:8000/product/id", data=data)
    return product_data.json()
from app.models.customers_model import database
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldbs = database()

def shows():
    dbsresult = mysqldbs.showUsers()
    result = []
    for items in dbsresult:
        user = {
            "id" : items[0],
            "username" : items[1],
            "name" : items[2],
            "useremail" : items[3],
            "useraddress" : items[4],
        }
        result.append(user)
    return jsonify(result)

def show(**params):
    dbsresult = mysqldbs.showUserById(**params)
    user = {
        "id" : dbsresult[0],
        "username" : dbsresult[1],
        "name" : dbsresult[2],
        "useremail" : dbsresult[3],
        "useraddress" : dbsresult[4]    
    }
    return jsonify(user)

def insert(**params):
    mysqldbs.insertUser(**params)
    mysqldbs.dataCommit()
    return jsonify({"massage":"Insert Success"})

def update(**params):
    mysqldbs.updateUserById(**params)
    mysqldbs.dataCommit()
    return jsonify({"massage":"Update Success"})

def delete(**params):
    mysqldbs.deleteUserById(**params)
    mysqldbs.dataCommit()
    return jsonify({"massage":"Delete Success"})

def token(**params):
    dbsresult = mysqldbs.showUserByEmail(**params)
    if dbsresult is not None:
        user = {
            "username" : dbsresult[1],
            "useremail" : dbsresult[4]
        }

        expires = datetime.timedelta(days=1)
        access_token =create_access_token(user, fresh=True, expires_delta=expires)

        data = {
            "data" : user,
            "token_access" : access_token
        }
    
    else:
        data = {
            "massage" : "Email tidak terdaftar"
        }
    return data
from mysql.connector import connect 

class database:
    
    def __init__(self):
        try:
            self.db = connect(host="localhost",
                            database="market_db",
                            user="root",
                            password="Mydata18_pass"
            )
        except Exception as e:
            print(e)

    def showOrdersByEmail(self,**params):
        cursor = self.db.cursor()
        email = params['email']
        query = f'''
            select custemers.username, orders.* from orders
            inner join customers on orders.userid = customers.id
            where customers.useremail = {email}.isready = 1;
        '''
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def insertOrder(self, **params):
        cursor = self.db.cursor()
        column = ','.join(list(params.keys()))
        values = tuple(list(params.values()))
        query = f'''
            insert into ({column})
            values {values};
        '''
        cursor.execute(query)

    def updateOrder(self, **params):
        cursor = self.db.cursor()
        orderid = params['orderid']
        values = self.restructureParams(**params['values'])
        query = f'''
            update orders set {values} 
            where customers.username = "{orderid}";
        '''
        cursor.execute(query)
        
    def commit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = [f'{item[0]} = "{item[1]}"'for item in data.items()]
        result = ', '.join(list_data)
        return result



from mysql.connector import connect

class database:
    def __init__(self):
        self.db = connect(host='localhost',
                              database='market_db',
                              user='root',
                              password='Mydata18_pass')
    
    def showUsers(self):
        query = '''select * from customers;'''
        cursor = self.db.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    
    def showUserById(self, **params):
        query = '''select * from customers where id = {0};'''.format(params['id'])
        cursor = self.db.cursor()
        cursor.execute(query)
        result =  cursor.fetchone()
        return result

    def showUserByEmail(self, **params):
        query = f'''select * from customers where useremail = '{params['useremail']}';'''  
        cursor = self.db.cursor()
        cursor.execute(query)
        result = cursor.fetchone()
        return result
    
    def insertUser(self, **params):
        column = ', '.join(list(params['values'].keys()))
        values = tuple(list(params['values'].values()))
        crud_query = f'''insert into customers ({column}) values {values};'''
        cursor = self.db.cursor()
        cursor.execute(crud_query)
    
    def updateUserById(self, **params):
            id = params['id']
            values = self.restructureParams(**params['values'])
            crud_query = f'''update customers set {values} where id = {id};'''
            cursor = self.db.cursor()
            cursor.execute(crud_query)
    
    def deleteUserById(self, **params):
            id = params['id']
            crud_query = f'''delete from customers where id = {id};'''
            cursor = self.db.cursor()
            cursor.execute(crud_query)
    
    def dataCommit(self):
        self.db.commit()
        
    def restructureParams(self, **data):
        list_data = [f'{item[0]} = "{item[1]}"' for item in data.items()]
        result = ', '.join(list_data)
        return result

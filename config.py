import os

dir_path = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    JSON_SHORT_KEY = False
    JWT_SECRET_KEY = str(os.environ.get('JWT_SECRET')) 